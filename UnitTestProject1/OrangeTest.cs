﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using System;

namespace UnitTestProject1
{
    [TestFixture]
    public class OrangeTest
    {
        IWebDriver driver;
        private object element;

        [SetUp]
        public void Setup()
        {
            driver = new ChromeDriver();
            driver.Url = "https://opensource-demo.orangehrmlive.com/";
        }

        [Test, Order(2)]
        public void LogIn()
        {
            driver.FindElement(By.XPath("//input[@id = 'txtUsername']")).SendKeys("Admin");
            driver.FindElement(By.XPath("//input[@id = 'txtPassword']")).SendKeys("admin123");
            driver.FindElement(By.XPath("//input[@id = 'btnLogin']")).Click();

            IWebElement welcomeAdmin = driver.FindElement(By.XPath("//a[@id = 'welcome']"));

            Actions moveMouse = new Actions(driver);
            moveMouse.MoveToElement(welcomeAdmin);
            Assert.IsNotNull(welcomeAdmin);
        }

        [Test, Order(1)] //order of the tests
        public void DashboardClick()
        {
            driver.FindElement(By.XPath("//input[@id = 'txtUsername']")).SendKeys("Admin");
            driver.FindElement(By.XPath("//input[@id = 'txtPassword']")).SendKeys("admin123");
            driver.FindElement(By.XPath("//input[@id = 'btnLogin']")).Click();

            IWebElement welcomeAdmin = driver.FindElement(By.XPath("//a[@id = 'welcome']"));

            Assert.IsNotNull(welcomeAdmin);
        }

        [TearDown]
        public void Teardown()
        {
            driver.Quit();
        }
    }
}
